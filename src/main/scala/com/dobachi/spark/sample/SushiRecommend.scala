package com.dobachi.spark.sample

import org.apache.spark.{SparkContext, SparkConf, Logging}
import org.apache.spark.SparkContext._
import org.apache.spark.rdd.RDD
import org.apache.spark.mllib.recommendation.{MatrixFactorizationModel, ALS, Rating}

import scala.sys.process._
import java.io.File


/**
 * Sushi Recommend
 */
object SushiRecommend {
  def main(args: Array[String]): Unit = {

    // Option parser
    val sushiRecommendOpParser = SushiRecommendOpParser()

    sushiRecommendOpParser.getParser().parse(args, SushiRecommendConfig()) exists { config =>

      val sparkConf = config.master match {
        case None => new SparkConf()
        case Some(x) => new SparkConf().setMaster(x)
      }

      config.appName match {
        case None =>
        case Some(x) => sparkConf.setAppName(x)
      }

      val sc = new SparkContext(sparkConf)

      val sushiRecommend = new SushiRecommend(sc,
                                              config.orderInput, config.netaInput, config.myOrder, config.outputPath,
                                              config.blockNum, config.rankNum, config.iterNum, config.lambda,
                                              config.implicitPrefs, config.alpha, config.nonnegative, config.seed)

      config.modeType match {
        case "recommend" => sushiRecommend.printRecommendationForUser()
        case "calMse" => sushiRecommend.printMSE()
        case "saveMse" => sushiRecommend.saveMSE()
      }

      sc.stop()
      true
    }
  }
}

@SerialVersionUID(1L)
class SushiRecommend(sc: SparkContext,
                     orderInput: String, netaInput: String, myOrder: Array[Int], outputPath: String,
                     blockNum: Int, rankNum: Int, iterNum: Int, lambda: Double,
                     implicitPrefs: Boolean, alpha: Double, nonnegative: Boolean, seed: Option[Int])
                    extends Serializable with Logging {


  val (userAndItemRate, userAndItemRateSize) = createInputData()
  val netaData = createNetaData()

  val model = createModel(userAndItemRate)

  def printRecommendationForUser() = {
    val recs = recommendForUser()
    recs.foreach(p => logInfo(s"Score: ${p._1}, Name: ${p._2}"))
  }

  def recommendForUser() ={
    val myUserId = userAndItemRateSize - 1
    val myRec = model.recommendProducts(myUserId.toInt, 20)
    val order = myOrder

    val myRecIdAndRate = sc.parallelize(myRec.map(p => (p.product, p.rating)))
    val myRecIdAndRateWithoutMyChoice = myRecIdAndRate.filter(p => order.contains(p._1) == false)

    val myRecWithName = myRecIdAndRateWithoutMyChoice.join(netaData).map(p => p._2)
    val sortedMyRecWithName = myRecWithName.sortByKey(false)
    sortedMyRecWithName.take(10)
  }

  def saveMSE() = {
    val mse = calcMSE()

    val f = new File(outputPath)
    s"echo ${mse}" #> f!
  }

  def printMSE() = {
    val mse = calcMSE()
    logInfo(s"MSE: ${mse}")
  }

  def calcMSE() = {
    val userAndItem = userAndItemRate.map { case Rating(user, item, rate) => (user, item)}
    val predictions = model.predict(userAndItem).map { case Rating(user, item, rate) => ((user, item), rate)}

    val ratesAndPreds = userAndItemRate.map { case Rating(user, item, rate) => ((user, item), rate)}.join(predictions)

    val MSE = ratesAndPreds.map { case ((user, item), (r1, r2)) => val err = (r1 - r2); err * err}.mean()

    MSE
  }

  def createInputData() = {
    val orderRDD = sc.textFile(orderInput)

    // Remove the first line of content,
    // because it is unnecessary for this application.
    val firstLine = sc.parallelize(Array(orderRDD.first))
    val notIncludeFirstLine = orderRDD.subtract(firstLine)

    // lines are split by space and unnecessary column is removed
    val originalContent = notIncludeFirstLine.map(p => p.split(' ').takeRight(10).map(_.toInt))

    // Add own content
    val paraMyContent = sc.parallelize(Array(myOrder))
    val content = originalContent.union(paraMyContent)

    val size = content.count()

    val contentWithIndex = content.zipWithIndex
    val userAndItemRate = contentWithIndex.flatMap { case (s, l) => s.zipWithIndex.map { case (p, r) => Rating(l.toInt, p.toInt, 10.0 - r)}}

    (userAndItemRate, size)
  }

  def createNetaData() = {
    val rawNetaData = sc.textFile(netaInput)
    val netaData = rawNetaData.map(p => p.split('\t').take(2)).map(p => (p(0).toInt, p(1)))

    netaData
  }

  def createModel(userAndItemRate: RDD[Rating]) = {
    val als = seed match {
       case None =>
         new ALS().setBlocks(blockNum).
                   setRank(rankNum).
                   setIterations(iterNum).
                   setLambda(lambda).
                   setImplicitPrefs(implicitPrefs).
                   setAlpha(alpha).
                   setNonnegative(nonnegative)
       case Some(x: Int) =>
         new ALS().setBlocks(blockNum).
                   setRank(rankNum).
                   setIterations(iterNum).
                   setLambda(lambda).
                   setImplicitPrefs(implicitPrefs).
                   setAlpha(alpha).
                   setNonnegative(nonnegative).
                   setSeed(x)
     }
    als.run(userAndItemRate)
  }


}