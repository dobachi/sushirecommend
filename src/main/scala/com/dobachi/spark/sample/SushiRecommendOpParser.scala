package com.dobachi.spark.sample

case class SushiRecommendConfig(orderInput: String = "",
                                netaInput: String = "",
                                myOrder: Array[Int] = Array(36, 22, 4, 20, 54, 32, 84, 64, 31, 2),
                                modeType: String = "recommend",
                                outputPath: String = "output",
                                blockNum: Int = -1,
                                rankNum: Int = 16,
                                iterNum: Int = 10,
                                lambda: Double = 0.01,
                                implicitPrefs: Boolean = false,
                                alpha: Double = 1.0,
                                nonnegative: Boolean = false,
                                seed: Option[Int] = None,
                                master: Option[String] = None,
                                appName: Option[String] = None)

/**
 * Option parser
 */
@SerialVersionUID(1L)
class SushiRecommendOpParser() extends Serializable{
  val parser = new scopt.OptionParser[SushiRecommendConfig]("SushiRecommend") {
    arg[String]("orderInput") required() action {
      (x, c) => c.copy(orderInput = x)
    } text("Path of input data of the user's order")

    arg[String]("netaInput") required() action {
      (x, c) => c.copy(netaInput = x)
    } text("Path of input data of kinds of Sushi")

    opt[String]('o', "myOrder") valueName("<10 of number string>") action {
      (x, c) => c.copy(myOrder = x.split(" ").map(_.toInt))
    } validate { x =>
      if(x.split(" ").map(_.toInt).forall(p => p >= 0 && p < 100)) success else failure("All numbers should be in range 0 ... 99")
    } text("My order of Sushi delimited by space. defualt: 36 22 4 20 54 32 84 64 31 2")

    opt[String]('t', "modeType") valueName("<string>") action {
      (x, c) => c.copy(modeType = x)
    } validate { x=>
      x match {
        case "recommend" => success
        case "saveRec" => success
        case "calMse" => success
        case "saveMse" => success
        case _ => failure("modeType should be 'recommend', 'saveRec', 'calMse' or 'saveMse'")
      }
    } text("Which function you execute. 'recommend', 'saveRec', 'calMse' or 'saveMse'")

    opt[String]('d', "outputPath") valueName("<string>") action {
      (x, c) => c.copy(outputPath = x)
    } text("Path of output")

    opt[Int]('b', "blockNum") valueName("<int number>") action {
      (x, c) => c.copy(blockNum = x)
    } text("Number of block which decides parallelism. default: -1")

    opt[Int]('r', "rankNum") valueName("<int number>") action {
      (x, c) => c.copy(rankNum = x)
    } text("Number of potential factor")

    opt[Int]('i', "iterNum") valueName("<int number>") action {
      (x, c) => c.copy(iterNum = x)
    } text("Number of iteration")

    opt[Double]('l', "lambda") valueName("<double number>") action {
      (x, c) => c.copy(lambda = x)
    } text("Regularisation parameter")

    opt[Unit]("implicitPrefs") action {
      (_, c) => c.copy(implicitPrefs = true)
    } text("Use implicit data as input")

    opt[Double]('a', "alpha") valueName("<dobule number>") action {
      (x, c) => c.copy(alpha = x)
    } text("Parameter for implicit iput data")

    opt[Unit]("nonnegative") action {
      (_, c) => c.copy(nonnegative = true)
    }

    opt[Int]('s', "seed") valueName("<int number>") action {
      (x, c) => c.copy(seed = Some(x))
    } text("Seed of random function. If you want reproducibility, you can use a static seed")

    opt[String]('m', "master") valueName("<string>") action {
      (x, c) => c.copy(master = Some(x))
    } text("Master URL of Spark cluster")

    opt[String]('n', "appName") valueName("<string>") action {
      (x, c) => c.copy(appName = Some(x))
    } text("Name of this application")
   }

  def getParser() = {
    parser
  }
}

object SushiRecommendOpParser {
  val sushiRecommendOpParser = new SushiRecommendOpParser()

  def apply() = {
    sushiRecommendOpParser
  }
}

