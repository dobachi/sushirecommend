###################
Sushi Recommend
###################
This is sample applicaiton of Spark MLlib.

*******************
Assumption
*******************
* You have installed sbt command

  + see: http://www.scala-sbt.org/

* You have HDSF/YARN cluster to execute application on YARN cluster.

**********
Prepare
**********

Input data
==========
Please Download input data from `Dr. Kamishima's web page <http://www.kamishima.net/sushi/>`_

Get data archives::

 $ cd
 $ mkdir Downloads
 $ cd ~/Downloads
 $ wget http://www.kamishima.net/asset/sushi3.tgz
 $ tar xvzf sushi3.tgz
 $ wget http://www.kamishima.net/asset/sushi3cf.tgz
 $ tar xvzf sushi3cf.tgz

Put data into HDFS::

 $ hdfs dfs -put sushi3/sushi3.idata
 $ hdfs dfs -put sushi3cf/sushi3b.5000.10.order

*******************
How to make jar
*******************
Clone this repository::

 $ cd
 $ mkdir Sources
 $ cd Sources
 $ git clone https://bitbucket.org/dobachi/sushirecommend.git SushiRecommend

Execute sbt::

 $ cd SushiRecommend
 $ sbt assembly

Now, you have JAR file the following path::

 ${HOME}/Sources/SushiRecommend/target/scala-2.10/sushirecommend.jar

**************************************************
How to submit application to YARN cluster
**************************************************
You can submit application with the following command::

 $ spark-submit --master yarn-client --class com.dobachi.spark.sample.SushiRecommend target/scala-2.10/sushirecommend.jar hdfs://mycluster/user/vagrant/sushi3b.5000.10.order hdfs://mycluster/user/vagrant/sushi3.idata


.. vim: ft=rst tw=0
