#!/bin/bash

BASE_DIR=${HOME}/MseTest
RANK_DIR=${BASE_DIR}/Rank
ITER_DIR=${BASE_DIR}/Iter
LAMBDA_DIR=${BASE_DIR}/Lambda

echo "Rank"
mkdir -p ${RANK_DIR}
for i in 2 4 8 16 32 64
do
  echo ${i}
  /usr/bin/spark-submit --master yarn-client --class com.dobachi.spark.sample.SushiRecommend target/scala-2.10/sushirecommend.jar hdfs://mycluster/user/vagrant/sushi3b.5000.10.order hdfs://mycluster/user/vagrant/sushi3.idata -t saveMse -d /${RANK_DIR}/mse_`printf "%03d" ${i}`.txt -r ${i}
done

echo "Iter"
mkdir -p ${ITER_DIR}
for i in 2 4 8 16 32
do
  echo ${i}
  /usr/bin/spark-submit --master yarn-client --class com.dobachi.spark.sample.SushiRecommend target/scala-2.10/sushirecommend.jar hdfs://mycluster/user/vagrant/sushi3b.5000.10.order hdfs://mycluster/user/vagrant/sushi3.idata -t saveMse -d /${ITER_DIR}/mse_`printf "%03d" ${i}`.txt -i ${i}
done

echo "Lambda"
mkdir -p ${LAMBDA_DIR}
for i in 0.005 0.01 0.05 0.1 0.5 1.0
do
  echo ${i}
  /usr/bin/spark-submit --master yarn-client --class com.dobachi.spark.sample.SushiRecommend target/scala-2.10/sushirecommend.jar hdfs://mycluster/user/vagrant/sushi3b.5000.10.order hdfs://mycluster/user/vagrant/sushi3.idata -t saveMse -d /${LAMBDA_DIR}/mse_${i}.txt -l ${i}
done
